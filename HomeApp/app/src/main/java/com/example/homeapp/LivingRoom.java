package com.example.homeapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class LivingRoom extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    List<room_item> LivingRoomDeviceList;

    RecyclerView recyclerView;
    RecyclerView recyclerViewSearchDevice;
    LivingRoomMenuAdapter adapter;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;

    DeviceListMenuAdapter deviceListMenuAdapter;
    String[] SeekItems;
    private String Network;
    private ArrayList<search_devices_item> mDeviceList = new ArrayList<search_devices_item>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.livingroom);

        //Set RecyclerView for Devices

        recyclerView = findViewById(R.id.rv_livingRoom);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadDeviceList();

        adapter = new LivingRoomMenuAdapter(this, LivingRoomDeviceList){};
        recyclerView.setAdapter(adapter);

        Toolbar toolbar = findViewById(R.id.living_room_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Living Room");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawer = findViewById(R.id.livingRoomDrawerLayout);

        SeekItems = getResources().getStringArray(R.array.search_network_items);

        NavigationView navigationView = findViewById(R.id.living_room_nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                //if(menuItem.isChecked()) menuItem.setChecked(false);
                //else menuItem.setChecked(true);
                //drawer.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.add_device:
                        AlertDialog.Builder builder = new AlertDialog.Builder(LivingRoom.this);
                        builder.setTitle("Choose how to search for devices:");

                        builder.setSingleChoiceItems(SeekItems, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Network = SeekItems[which];
                            }
                        });

                        // Set up the buttons
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                boolean ret = false;
                                AlertDialog.Builder searchNetworkDialog = new AlertDialog.Builder(LivingRoom.this);
                                searchNetworkDialog.setTitle("List of " + Network + " devices: ");
                                //searchNetworkDialog.setTitle(Network);
                                View customLayout = getLayoutInflater().inflate(R.layout.search_devices_layout, null);
                                searchNetworkDialog.setView(customLayout);

                                recyclerViewSearchDevice = customLayout.findViewById(R.id.deviceListView);
                                deviceListMenuAdapter = new DeviceListMenuAdapter(LivingRoom.this, mDeviceList) {};
                                recyclerViewSearchDevice.setLayoutManager(new LinearLayoutManager(getApplication().getApplicationContext()));
                                recyclerViewSearchDevice.setAdapter(deviceListMenuAdapter);

                                recyclerViewSearchDevice.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                                    GestureDetector gestureDetector = new GestureDetector(LivingRoom.this, new GestureDetector.SimpleOnGestureListener() {

                                        @Override public boolean onSingleTapUp(MotionEvent motionEvent) {

                                            return true;
                                        }

                                    });
                                    @Override
                                    public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                                        View ChildView = rv.findChildViewUnder(e.getX(), e.getY());

                                        if(ChildView != null && gestureDetector.onTouchEvent(e)) {
                                            // Get position
                                            int RecyclerViewItemPosition = rv.getChildAdapterPosition(ChildView);
                                            // add device to list
                                            addDevice(R.drawable.ic_lightbulb_outline_black_24dp, mDeviceList.get(RecyclerViewItemPosition).getName(), mDeviceList.get(RecyclerViewItemPosition).getAddress());
                                        }
                                        return false;
                                    }

                                    @Override
                                    public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {

                                    }

                                    @Override
                                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                                    }
                                });

                                if(Network.equals("Bluetooth")) {
                                    ret = searchBluetooth();
                                }
                                if(Network.equals("WiFi")) {
                                    ret = searchWifi();
                                }

                                if (ret) {
                                    searchNetworkDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                                    searchNetworkDialog.show();
                                }
                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                        builder.show();
                        drawer.closeDrawers();
                        break;
                    case R.id.Settings:
                        drawer.closeDrawers();
                        break;
                }
                return true;
            }
        });
    }

    private boolean searchWifi() {
        Toast.makeText(getApplicationContext(), "Searching WiFi network..", Toast.LENGTH_SHORT).show();
        mDeviceList.clear();
        addSearchDevice("No Device Found", "");
        return true;
    }

    private boolean searchBluetooth() {
        boolean ret;
        BluetoothAdapter BTAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!BTAdapter.isEnabled()) {
            Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(i);
            Toast.makeText(getApplicationContext(), "START DISCOVERY...", Toast.LENGTH_SHORT).show();
            ret = BTAdapter.startDiscovery();
        } else {
            Toast.makeText(getApplicationContext(), "START DISCOVERY...", Toast.LENGTH_SHORT).show();
            ret = BTAdapter.startDiscovery();
        }


        if(ret) {
            BroadcastReceiver BCReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        BluetoothDevice BTDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        Toast.makeText(getApplicationContext(), BTDevice.getName() + "||" + BTDevice.getAddress(), Toast.LENGTH_SHORT).show();
                        addSearchDevice(BTDevice.getName(), BTDevice.getAddress());
                    } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                        Toast.makeText(getApplicationContext(), "No New Devices Found", Toast.LENGTH_SHORT).show();
                    }
                }
            };
            IntentFilter ifr = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            registerReceiver(BCReceiver, ifr);
        }
        return true;
    }

    private void saveDeviceList() {
        sharedPreferences = getApplicationContext().getSharedPreferences("LivingRoomDevicePref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(LivingRoomDeviceList);
        editor.putString("LivingRoomDeviceList", json);
        editor.apply();
    }

    public void loadDeviceList(){
        sharedPreferences = getApplicationContext().getSharedPreferences("LivingRoomDevicePref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LivingRoomDeviceList", null);
        Type type = new TypeToken<ArrayList<room_item>>() {}.getType();
        LivingRoomDeviceList = gson.fromJson(json, type);

        if(LivingRoomDeviceList == null) {
            LivingRoomDeviceList = new ArrayList<>();
        }
    }

    private void addDevice(int icon, String name, String address) {
        LivingRoomDeviceList.add(new room_item(icon, name, address));
        saveDeviceList();
        adapter.notifyDataSetChanged();
    }

    private void addSearchDevice(String name, String address) {
            mDeviceList.add(new search_devices_item(name, address));
            deviceListMenuAdapter.notifyDataSetChanged();
    }
}