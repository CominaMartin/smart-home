package com.example.homeapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.lang.reflect.Type;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment_Calender.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment_Calender#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment_Calender extends Fragment {

    SharedPreferences sharedPreferences;
    CalenderEventMenuAdapter adapter;
    String[] eventColourItems;
    RecyclerView recyclerView;

    int listSelect;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String SelectedDate = "";
    private Date SelectedSaveDate;
    private String colour;
    ArrayList<event_item> EventList;
    ArrayList<event_item> ActiveEventList;

    private OnFragmentInteractionListener mListener;

    public DashboardFragment_Calender() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment_Calender newInstance(String param1, String param2) {
        DashboardFragment_Calender fragment = new DashboardFragment_Calender();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.fragment_dashboard_calender, container, false);

        recyclerView = v.findViewById(R.id.rv_calender);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        Date CurrentDate = new Date(); // this object contains the current date value
        SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");
        final String CurrentDateString = formatter.format(CurrentDate);
        SelectedDate = CurrentDateString;

        loadEventList();
        loadActiveEventList();
        if(ActiveEventList != null) {
            ActiveEventList.clear();
        }
        /*
        for(int i = 0; i < EventList.size(); i++) {
            if (EventList.get(i).getDate().equals(CurrentDate)) {
                Toast.makeText(getActivity(), EventList.get(i).getDate(), Toast.LENGTH_SHORT).show();
                Toast.makeText(getActivity(), CurrentDate, Toast.LENGTH_SHORT).show();
                addActiveEvent(EventList.get(i).getColour(), EventList.get(i).getName(), EventList.get(i).getDate());
            }
        }
        */
        adapter = new CalenderEventMenuAdapter(getActivity(), ActiveEventList) {};
        recyclerView.setAdapter(adapter);

        Spinner listSelectSpinner = v.findViewById(R.id.listSelectSpinner);
        listSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: // Show only current date
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            if (myItem.getDate() != null && myItem.getDate().equals(SelectedDate)) {
                                addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 0;
                        break;
                    case 1: // Add complete list
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 1;
                        break;
                    case 2: // Show specific events
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            if (myItem.getColour() != null && myItem.getColour().equals("Task")) {
                                addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 2;
                        break;
                    case 3: // Show specific events
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            if (myItem.getColour() != null && myItem.getColour().equals("Appointment")) {
                                addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 3;
                        break;
                    case 4: // Show specific events
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            if (myItem.getColour() != null && myItem.getColour().equals("Deadline")) {
                                addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 4;
                        break;
                    case 5: // Show specific events
                        ActiveEventList.clear();
                        for(event_item myItem : EventList) {
                            if (myItem.getColour() != null && myItem.getColour().equals("Birthday")) {
                                addActiveEvent(myItem.getColour(), myItem.getName(), myItem.getDate(), myItem.getSavedDate());
                            }
                        }
                        adapter.notifyDataSetChanged();
                        listSelect = 5;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        eventColourItems = getResources().getStringArray(R.array.event_colours_items);

        final MaterialCalendarView calendar = v.findViewById(R.id.calendarView);
        calendar.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
                SelectedDate = dateFormat.format(date.getDate());
                try {
                    SelectedSaveDate = dateFormat.parse(SelectedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(listSelect == 0) {
                    ActiveEventList.clear();

                    for (int i = 0; i < EventList.size(); i++) {
                        if (EventList.get(i).getDate().equals(SelectedDate)) {
                            addActiveEvent(EventList.get(i).getColour(), EventList.get(i).getName(), EventList.get(i).getDate(), EventList.get(i).getSavedDate());
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                //deleteEvent(viewHolder.getAdapterPosition());
                event_item item = ActiveEventList.get(viewHolder.getAdapterPosition());
                deleteActiveEvent(viewHolder.getAdapterPosition());
                for (int i = 0; i < EventList.size(); i++) {
                    if (EventList.get(i).getName() != null && EventList.get(i).getName().equals(item.getName()) && EventList.get(i).getDate() != null && EventList.get(i).getDate().equals(item.getDate())){
                        int EventListPosition = EventList.indexOf(EventList.get(i));
                        deleteEvent(EventListPosition);
                    }
                }
                adapter.notifyDataSetChanged();
                reloadEventIcons(calendar);
            }
        };
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        reloadEventIcons(calendar);

        /*
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String date = dayOfMonth + "/" + (month + 1) + "/" + year;
                SelectedDate = date;
                SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
                try {
                    SelectedSaveDate = dateFormat.parse(SelectedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if(listSelect == 0) {
                    ActiveEventList.clear();

                    for (int i = 0; i < EventList.size(); i++) {
                        if (EventList.get(i).getDate().equals(SelectedDate)) {
                            addActiveEvent(EventList.get(i).getColour(), EventList.get(i).getName(), EventList.get(i).getDate(), EventList.get(i).getSavedDate());
                        }
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });

         */

        FloatingActionButton todoAddButton = v.findViewById(R.id.calenderEventAddButton);
        todoAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Enter Event: ");

                builder.setSingleChoiceItems(eventColourItems, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        colour = eventColourItems[which];
                    }
                });
// Set up the input
                final EditText input = new EditText(getActivity());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

// Set up the buttons
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String EventText = input.getText().toString();
                        if (!(SelectedDate.isEmpty())){
                            // Set event
                            addEvent(colour, EventText, SelectedDate, SelectedSaveDate);
                            addActiveEvent(colour, EventText, SelectedDate, SelectedSaveDate);
                            adapter.notifyDataSetChanged();
                            reloadEventIcons(calendar);
                        } else {
                            Toast.makeText(getActivity(), "Select a date!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });
        return v;
    }

    private void saveEventList() {
        sharedPreferences = getActivity().getSharedPreferences("EventPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(EventList);
        editor.putString("EventList", json);
        editor.apply();
    }

    public void loadEventList(){
        sharedPreferences = getActivity().getSharedPreferences("EventPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("EventList", null);
        Type type = new TypeToken<ArrayList<event_item>>() {}.getType();
        EventList = gson.fromJson(json, type);

        if(EventList == null) {
            EventList = new ArrayList<>();
        }
    }

    private void addEvent(String colour, String name, String date, Date savedDate) {
        int set = 0;
        for(int i = 0; i < EventList.size(); i++) {
            if(savedDate.compareTo(EventList.get(i).getSavedDate()) < 0) {
                EventList.add(i, new event_item(colour, name, date, savedDate));
                set = 1;
                break;
            }
        }
        if(set == 0) {
            EventList.add(new event_item(colour, name, date, savedDate));
        }
        saveEventList();
        //adapter.notifyDataSetChanged();
    }

    private void deleteEvent(int position) {
        EventList.remove(position);
        saveEventList();
        //adapter.notifyDataSetChanged();
    }

    private void saveActiveEventList() {
        sharedPreferences = getActivity().getSharedPreferences("ActiveEventPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(ActiveEventList);
        editor.putString("ActiveEventList", json);
        editor.apply();
    }

    public void loadActiveEventList(){
        /*
        sharedPreferences = getActivity().getSharedPreferences("ActiveEventPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("ActiveEventList", null);
        Type type = new TypeToken<ArrayList<event_item>>() {}.getType();
        ActiveEventList = gson.fromJson(json, type);
        */
        if(ActiveEventList == null) {
            ActiveEventList = new ArrayList<>();
        }
    }

    private void addActiveEvent(String colour, String name, String date, Date savedDate) {
        ActiveEventList.add(new event_item(colour, name, date, savedDate));
        //saveActiveEventList();
        adapter.notifyDataSetChanged();
    }

    private void deleteActiveEvent(int position) {
        ActiveEventList.remove(position);
        //adapter.notifyDataSetChanged();
    }

    private void reloadEventIcons(MaterialCalendarView materialCalendarView) {
        int color = 0;
        for(int i = 0; i < EventList.size(); i++) {
            if(EventList.get(i).getColour().equals("Task")) {
                //color = R.color.eventGreen;
                color = Color.GREEN;
            }
            if(EventList.get(i).getColour().equals("Appointment")) {
                //color = R.color.eventBlack;
                color = Color.BLACK;
            }
            if(EventList.get(i).getColour().equals("Deadline")) {
                //color = R.color.eventRed;
                color = Color.RED;
            }
            if(EventList.get(i).getColour().equals("Birthday")) {
                //color = R.color.eventBlue;
                color = Color.BLUE;
            }
            EventDecorator eventDecorator = new EventDecorator(color, EventList.get(i).getSavedDate());
            materialCalendarView.addDecorator(eventDecorator);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
