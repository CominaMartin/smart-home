package com.example.homeapp;

public class todo_item {

    private int icon;
    private String task;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return task;
    }

    public void setName(String name) {
        this.task = name;
    }

    public todo_item(int icon, String name) {
        this.icon = icon;
        this.task = name;
    }
}
