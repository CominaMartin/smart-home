package com.example.homeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.List;

public abstract class LivingRoomMenuAdapter extends RecyclerView.Adapter<LivingRoomMenuAdapter.MenuViewHolder> {


    Context mContext;
    List<room_item> mData;

    public LivingRoomMenuAdapter(Context mContext, List<room_item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.room_item_layout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.roomDeviceIcon.setImageResource(mData.get(position).getIcon());
        holder.roomDeviceName.setText(mData.get(position).getName());
        holder.roomLightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    SwitchLightOn();
                }
                if(!isChecked){
                    SwitchLightOff();
                }
            }
        });
    }

    public void SwitchLightOn(){
        Toast.makeText(mContext, "ON", Toast.LENGTH_SHORT).show();
    }

    public void SwitchLightOff(){
        Toast.makeText(mContext, "OFF", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected ImageView roomDeviceIcon;
        protected TextView roomDeviceName;
        protected Switch roomLightSwitch;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            roomDeviceIcon = itemView.findViewById(R.id.room_device_icon);
            roomDeviceName = itemView.findViewById(R.id.room_device_name);
            roomLightSwitch = itemView.findViewById(R.id.lightSwitch);
        }
    }
}
