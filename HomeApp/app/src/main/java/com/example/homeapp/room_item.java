package com.example.homeapp;

public class room_item {

    private int icon;
    private String device;
    private String address;

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getName() {
        return device;
    }

    public void setName(String name) {
        this.device = name;
    }

    public String getAddress() {return address;}

    public void setAddress(String address) { this.address = address; }

    public room_item(int icon, String name, String address) {
        this.icon = icon;
        this.device = name;
        this.address = address;
    }
}
