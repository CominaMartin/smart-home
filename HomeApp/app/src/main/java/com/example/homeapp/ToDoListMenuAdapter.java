package com.example.homeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class ToDoListMenuAdapter extends RecyclerView.Adapter<ToDoListMenuAdapter.MenuViewHolder> {


    Context mContext;
    //ArrayList<ArrayList<list_item>> mData;
    LinkedHashMap<String, ArrayList<list_item>> mData;

    DashboardFragment_ToDo.OnItemClicked onClick;

    public ToDoListMenuAdapter(Context mContext, LinkedHashMap<String, ArrayList<list_item>> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todo_linkedhashmap_list_layout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuViewHolder holder, final int position) {
        int i = 0;
        for(String key : mData.keySet())
        {
            if (i == position) {
                holder.todoTask.setText(key);
            }
            i++;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(position);
            }
        });
        // Set Delete Button
        holder.DeleteButton.setTag(position);
        holder.DeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int positionDelete;
                positionDelete =(Integer)v.getTag();
                onClick.onDeleteClick(positionDelete);
            }
        });
    }

    public void setOnClick(DashboardFragment_ToDo.OnItemClicked onClick) {
        this.onClick = onClick;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected TextView todoTask;
        protected ImageButton DeleteButton;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            todoTask = itemView.findViewById(R.id.todo_task);
            DeleteButton = itemView.findViewById(R.id.DeleteListButton);
        }
    }
}
