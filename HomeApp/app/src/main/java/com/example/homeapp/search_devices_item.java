package com.example.homeapp;

public class search_devices_item {

    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public search_devices_item(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
