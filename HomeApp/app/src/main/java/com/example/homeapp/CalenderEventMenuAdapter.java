package com.example.homeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class CalenderEventMenuAdapter extends RecyclerView.Adapter<CalenderEventMenuAdapter.MenuViewHolder> {


    Context mContext;
    List<event_item> mData;

    public CalenderEventMenuAdapter(Context mContext, List<event_item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.event_item_layout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.CalenderEvent.setText(mData.get(position).getName());
        holder.CalenderEventDate.setText(mData.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected TextView CalenderEvent;
        protected TextView CalenderEventDate;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            CalenderEvent = itemView.findViewById(R.id.event_task);
            CalenderEventDate = itemView.findViewById(R.id.event_date);
        }
    }
}
