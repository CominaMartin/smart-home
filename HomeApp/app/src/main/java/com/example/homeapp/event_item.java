package com.example.homeapp;

import java.util.Date;

public class event_item {

    public String date;
    public Date SavedDate;
    private String event;
    private String colour;

    public String getColour() { return colour; }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getName() {
        return event;
    }

    public void setName(String name) {
        this.event = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {this.date = date; }

    public Date getSavedDate() {return SavedDate; }

    public void setSavedDate(Date SavedDate) {this.SavedDate = SavedDate; }

    public event_item(String colour, String name, String date, Date savedDate) {
        this.colour = colour;
        this.event = name;
        this.date = date;
        this.SavedDate = savedDate;
    }
}
