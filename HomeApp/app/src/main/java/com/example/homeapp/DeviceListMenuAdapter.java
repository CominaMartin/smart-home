package com.example.homeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public abstract class DeviceListMenuAdapter extends RecyclerView.Adapter<DeviceListMenuAdapter.MenuViewHolder> {


    Context mContext;
    List<search_devices_item> mData;

    public DeviceListMenuAdapter(Context mContext, List<search_devices_item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.search_devices_item_layout, parent, false);

        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuViewHolder holder, final int position) {
        holder.DeviceName.setText(mData.get(position).getName());
        holder.DeviceAddress.setText(mData.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected TextView DeviceName;
        protected TextView DeviceAddress;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            DeviceName = itemView.findViewById(R.id.search_device_name);
            DeviceAddress = itemView.findViewById(R.id.search_device_address);
        }
    }
}
