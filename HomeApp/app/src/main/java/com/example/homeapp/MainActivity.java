package com.example.homeapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.MainDrawerLayout);

        NavigationView navigationView = findViewById(R.id.main_menu_nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Home()).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.Home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Home()).commit();
                break;
            //case R.id.Rooms:
              //  getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Rooms()).commit();
                //break;
            case R.id.To_Do:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_ToDo()).commit();
                break;
            case R.id.Calender:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Calender()).commit();
                break;
            case R.id.Settings:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Settings()).commit();
                break;
            case R.id.Profile:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new DashboardFragment_Profile()).commit();
                break;
        }

        drawerLayout.closeDrawer(GravityCompat.END);
        return true;
    }
}
