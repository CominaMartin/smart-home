package com.example.homeapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment_ToDo.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment_ToDo#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment_ToDo extends Fragment {

    SharedPreferences sharedPreferences;

    String[] ChooseItems;
    private String Item;

    ToDoMenuAdapter adapter;
    ToDoListMenuAdapter ListAdapter;
    RecyclerView recyclerViewTodo;
    RecyclerView recyclerViewList;
    RecyclerView recyclerViewHashmapList;
    ListMenuAdapter ListMenuAdapter;
    ArrayList<todo_item> todoList;
    LinkedHashMap<String, ArrayList<list_item>> todoListList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DashboardFragment_ToDo() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment_ToDo newInstance(String param1, String param2) {
        DashboardFragment_ToDo fragment = new DashboardFragment_ToDo();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.fragment_dashboard_todo, container, false);

        recyclerViewTodo = v.findViewById(R.id.rv_todo);
        recyclerViewTodo.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        loadTodoList();

        adapter = new ToDoMenuAdapter(getActivity(), todoList) {};

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                deleteTask(viewHolder.getAdapterPosition());
            }
        };
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerViewTodo);

        recyclerViewTodo.setAdapter(adapter);

        // Set List RecycleViewer

        recyclerViewList = v.findViewById(R.id.rv_todo_list);
        recyclerViewList.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        loadList();

        ListAdapter = new ToDoListMenuAdapter(getActivity(), todoListList) {};

        ListAdapter.setOnClick(new OnItemClicked() {
            @Override
            public void onItemClick(int position) {
                int i = 0;
                for(final String keys : todoListList.keySet()) {
                    if(i == position) {
                        AlertDialog.Builder ListViewBuilder = new AlertDialog.Builder(getActivity());
                        ListViewBuilder.setTitle(keys);

                        View customListView = getLayoutInflater().inflate(R.layout.todo_list_layout, null);
                        ListViewBuilder.setView(customListView);

                        if(!(todoListList.isEmpty()) && todoListList.get(keys) != null) {
                            if (todoListList.get(keys).isEmpty()) {
                                todoListList.get(keys).add(0, new list_item("List is empty"));
                            }
                        }

                        recyclerViewHashmapList = customListView.findViewById(R.id.TodoListView);
                        ListMenuAdapter = new ListMenuAdapter(getActivity(), todoListList.get(keys)) {};
                        recyclerViewHashmapList.setLayoutManager(new LinearLayoutManager(getActivity()));
                        recyclerViewHashmapList.setAdapter(ListMenuAdapter);

                        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                            @Override
                            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                                return false;
                            }

                            @Override
                            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                                deleteItemFromList(keys, viewHolder.getAdapterPosition());
                            }
                        };
                        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerViewHashmapList);


                        ListViewBuilder.setPositiveButton("Add Item", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AlertDialog.Builder ListItemBuilder = new AlertDialog.Builder(getActivity());
                                ListItemBuilder.setTitle("Set Item: ");

                                final EditText input = new EditText(getActivity());

                                input.setInputType(InputType.TYPE_CLASS_TEXT);
                                ListItemBuilder.setView(input);

                                ListItemBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String ListText = input.getText().toString();
                                        addItemToList(keys, ListText);
                                    }
                                });
                                ListItemBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                    }
                                });
                                ListItemBuilder.show();

                            }
                        });
                        ListViewBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        ListViewBuilder.show();
                    }
                    i++;
                }
            }

            @Override
            public void onDeleteClick(int position) {
                int i = 0;
                for(String key : todoListList.keySet()) {
                    if(i == position) {
                        deleteList(key);
                        break;
                    }
                    i++;
                }
            }
        });
        recyclerViewList.setAdapter(ListAdapter);

        // Set Floating Action Button

        ChooseItems = getResources().getStringArray(R.array.create_task_list_items);

        FloatingActionButton todoAddButton = v.findViewById(R.id.todo_add_button);
        todoAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                //builder.setTitle("Enter Task: ");
                builder.setTitle("Create: ");

                builder.setSingleChoiceItems(ChooseItems, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Item = ChooseItems[which];
                    }
                });

                final EditText input = new EditText(getActivity());

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String taskText = input.getText().toString();
                        if(!(taskText.isEmpty())) {
                            if (Item.equals("To-Do")) {
                                addTask(taskText);
                            } else {
                                addList(taskText);
                            }
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        return v;
    }

    public interface OnItemClicked {
        void onItemClick(int position);
        void onDeleteClick(int position);
    }

    private void saveTodoList() {
        sharedPreferences = getActivity().getSharedPreferences("todoPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(todoList);
        editor.putString("todoList", json);
        editor.apply();
    }

    private void loadTodoList() {
        sharedPreferences = getActivity().getSharedPreferences("todoPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("todoList", null);
        Type type = new TypeToken<ArrayList<todo_item>>() {}.getType();
        todoList = gson.fromJson(json, type);

        if(todoList == null) {
            todoList = new ArrayList<>();
        }
    }

    private void saveList() {
        sharedPreferences = getActivity().getSharedPreferences("LinkedHashMapListPref", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(todoListList);
        editor.putString("LinkedHashMapList", json);
        editor.apply();
    }

    private void loadList() {
        sharedPreferences = getActivity().getSharedPreferences("LinkedHashMapListPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LinkedHashMapList", null);
        Type type = new TypeToken<LinkedHashMap<String, ArrayList<list_item>>>(){}.getType();
        todoListList = gson.fromJson(json, type);


        if(todoListList == null) {
            todoListList = new LinkedHashMap<>();
        }
    }

    private void addList(String name) {
        ArrayList<list_item> list = new ArrayList<>();
        todoListList.put(name, list);
        saveList();
        ListAdapter.notifyDataSetChanged();
    }

    public void deleteList(String name) {
        todoListList.remove(name);
        saveList();
        ListAdapter.notifyDataSetChanged();
    }

    private void addItemToList(String listKey, String name) {
        if((todoListList.containsKey(listKey)) && (todoListList.get(listKey) != null)) {
            if(todoListList.get(listKey).get(0).getName().equals("List is empty")) {
                todoListList.get(listKey).remove(0);
            }
            todoListList.get(listKey).add(new list_item(name));
            saveList();
            ListAdapter.notifyDataSetChanged();
        }
    }

    private void deleteItemFromList(String listKey, int position) {
        todoListList.get(listKey).remove(position);
        saveList();
        ListAdapter.notifyDataSetChanged();
        ListMenuAdapter.notifyDataSetChanged();
        if(todoListList.get(listKey).isEmpty()) {
            todoListList.get(listKey).add(0, new list_item("List is empty"));
        }
    }

    private void addTask(String name) {
        todoList.add(new todo_item(R.drawable.ic_event_black_24dp, name));
        saveTodoList();
        adapter.notifyDataSetChanged();
    }

    private void deleteTask(int position) {
        todoList.remove(position);
        saveTodoList();
        adapter.notifyDataSetChanged();
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
