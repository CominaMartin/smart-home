package com.example.homeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class ListMenuAdapter extends RecyclerView.Adapter<ListMenuAdapter.MenuViewHolder> {


    Context mContext;
    ArrayList<list_item> mData;

    public ListMenuAdapter(Context mContext, ArrayList<list_item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todo_list_item_layout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.todoTask.setText(mData.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected TextView todoTask;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            todoTask = itemView.findViewById(R.id.todo_task);
        }
    }
}
