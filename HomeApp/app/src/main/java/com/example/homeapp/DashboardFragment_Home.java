package com.example.homeapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.diegodobelo.expandingview.ExpandingItem;
import com.diegodobelo.expandingview.ExpandingList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment_Home.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment_Home#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment_Home extends Fragment {

    SharedPreferences sharedPreferences;
    private CardView cardTop, cardBottom, cardLeft, cardRight;
    ArrayList<event_item> EventList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public DashboardFragment_Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment_Home newInstance(String param1, String param2) {
        DashboardFragment_Home fragment = new DashboardFragment_Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup v = (ViewGroup) inflater.inflate(R.layout.fragment_dashboard_home, container, false);

        loadEventList();

        cardTop = v.findViewById(R.id.cardTop);
        cardRight = v.findViewById(R.id.cardRight);
        cardLeft = v.findViewById(R.id.cardLeft);
        cardBottom = v.findViewById(R.id.cardBottom);

        //ini Animation

        Animation animBottomToTop = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_bottom_to_top);
        Animation animTopToBottom = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_top_to_bottom);
        Animation animLeftToRight = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_left_to_right);
        Animation animRightToLeft = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_right_to_left);


        cardBottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        cardTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LivingRoom.class);
                startActivity(intent);
            }
        });

        cardLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        cardRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        // setup Animation

        cardBottom.setAnimation(animBottomToTop);
        cardTop.setAnimation(animTopToBottom);
        cardLeft.setAnimation(animLeftToRight);
        cardRight.setAnimation(animRightToLeft);


        ExpandingList expandingList = v.findViewById(R.id.today_list_view);
        expandingList.setAnimation(animTopToBottom);

        ExpandingItem item = expandingList.createNewItem(R.layout.today_list_item_layout);


        /*ExpandingItem extends from View, so you can call
        findViewById to get any View inside the layout*/
        TextView text_item = item.findViewById(R.id.title);
        text_item.setText("Today");
        text_item.setTextColor(getResources().getColor(R.color.white));

        int DateCounter = 0;
        int TaskCounter = 0;

        if(EventList != null) {
            Date CurrentDate = new Date(); // this object contains the current date value
            SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");
            String CurrentDateString = formatter.format(CurrentDate);

            for(int j = 0; j < EventList.size(); j++) {
                if(EventList.get(j).getDate() != null && EventList.get(j).getDate().equals(CurrentDateString)) {
                    DateCounter++;
                }
            }
            if(DateCounter > 0)
            {
                item.createSubItems(DateCounter);
                for(int i = 0; i < EventList.size(); i++) {
                    if(EventList.get(i).getDate() != null && EventList.get(i).getDate().equals(CurrentDateString)) {
                        View subItemZero = item.getSubItemView(TaskCounter);
                        ((TextView) subItemZero.findViewById(R.id.sub_title)).setText(EventList.get(i).getName());
                        ((TextView) subItemZero.findViewById(R.id.sub_title)).setTextColor(getResources().getColor(R.color.white));
                        TaskCounter++;
                    }
                }
            } else {
                item.createSubItems(1);
                View subItem = item.getSubItemView(0);
                ((TextView) subItem.findViewById(R.id.sub_title)).setText("Nothing to do!");
                ((TextView) subItem.findViewById(R.id.sub_title)).setTextColor(getResources().getColor(R.color.white));
            }
        }

        item.setIndicatorColorRes(R.color.dark);
        item.setIndicatorIconRes(R.drawable.ic_add_black_24dp);

        // Inflate the layout for this fragment
        return v;
    }

    public void loadEventList(){
        sharedPreferences = getActivity().getSharedPreferences("EventPref", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("EventList", null);
        Type type = new TypeToken<ArrayList<event_item>>() {}.getType();
        EventList = gson.fromJson(json, type);

        if(EventList == null) {
            EventList = new ArrayList<>();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
