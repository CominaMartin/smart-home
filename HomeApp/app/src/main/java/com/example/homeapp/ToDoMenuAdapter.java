package com.example.homeapp;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public abstract class ToDoMenuAdapter extends RecyclerView.Adapter<ToDoMenuAdapter.MenuViewHolder> {


    Context mContext;
    List<todo_item> mData;

    public ToDoMenuAdapter(Context mContext, List<todo_item> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todo_item_layout, parent, false);
        return new MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.todoTask.setText(mData.get(position).getName());
        holder.todoIcon.setImageResource(mData.get(position).getIcon());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        protected ImageView todoIcon;
        protected TextView todoTask;

        public MenuViewHolder(@NonNull final View itemView) {
            super(itemView);
            todoIcon = itemView.findViewById(R.id.todo_icon);
            todoTask = itemView.findViewById(R.id.todo_task);
        }
    }
}
